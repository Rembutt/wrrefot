﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementVertical : MonoBehaviour
{
    [Range(0f, 5f)]
    public float Speed;

    [Range(0f, 10f)]
    public float MovementSize;

    bool GoUp;
    bool GoDown;

    void Start()
    {
        GoUp = true;
    }
    void Update()
    {
        Vector3 move = new Vector3(0, Speed, 0);

        if (transform.position.y >= MovementSize)
        {
            GoUp = false;
            GoDown = true;
        }
        if (transform.position.y <= -MovementSize)
        {
            GoUp = true;
            GoDown = false;
        }
        if (GoUp)
        {
            transform.position += move * Time.deltaTime;
        }
        else if (GoDown)
        {
            transform.position -= move * Time.deltaTime;
        }
        else
        {
            Debug.Log("Position error");
        }




    }
}