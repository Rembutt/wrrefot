﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [Range(0f, 5f)]
    public float Speed;

    [Range (0f, 10f)]
    public float MovementSize;

    private bool toRight;
    private bool toLeft;

    void Start()
    {
        toRight = true;  
    }
    void Update()
    {
        Vector3 move = new Vector3(Speed, 0, 0);

        if(transform.position.x >= MovementSize)
        {
            toRight = false;
            toLeft = true;
        }
        if (transform.position.x <= - MovementSize)
        {
            toRight = true;
            toLeft = false;
        }
        if (toRight)
        {
            transform.position += move * Time.deltaTime;
        }
        else if (toLeft)
        {
            transform.position -= move * Time.deltaTime;
        }
        else
        {
            Debug.Log("Position error");
        }
            

            

    }
}
