﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Rigidbody player;
    public float speed;

    private void Update()
    {
        Move();
    }

    private void Move()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        if (moveVertical < 0)
        {
            moveVertical *= -1;
        }

        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);
        player.AddForce(movement * speed);
    }
}
